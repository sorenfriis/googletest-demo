#include "m1.h"
#include "gtest/gtest.h"

namespace {

// The fixture for testing class Foo.
class FooTest : public ::testing::Test {
};

TEST(M1Test, Returns4) {
  EXPECT_EQ(m1_function1(), 4);
}

TEST(M1Test, ReturnsNot5) {
  EXPECT_NE(m1_function2(), 5);
}

TEST(M1Test, ReturnsGreatherThan4) {
  EXPECT_GT(m1_function2(), 4);
}

TEST(M1Test, Returns8) {
  EXPECT_EQ(m1_function2(), 8);
}

}  // namespace
