add_library(m1
  src/m1.cpp
)

target_include_directories(m1
  PUBLIC inc
)

if(BUILD_UNIT_TESTS)
  add_subdirectory(test)
endif()
