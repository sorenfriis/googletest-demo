#include "m2.h"
#include "gtest/gtest.h"

namespace {

// The fixture for testing class Foo.
class M2Test : public ::testing::Test {
  protected:
};

TEST_F(M2Test, a) {
  EXPECT_EQ(m2_function1(), 1);
}

TEST_F(M2Test, b) {
  EXPECT_EQ(m2_function2(), 2);
}

}  // namespace
