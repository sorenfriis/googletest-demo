#!/bin/sh

docker run -it --rm \
    --user $(id -u):$(id -g) \
    --volume $(pwd)/..:/home \
    --volume ccache:/ccache \
    --volume /etc/group:/etc/group:ro \
    --volume /etc/passwd:/etc/passwd:ro \
    --volume /etc/shadow:/etc/shadow:ro \
    test
