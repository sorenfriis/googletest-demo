#include <iostream>

using std::cout;
using std::endl;

#include "m1.h"
#include "m2.h"

int main(){
    cout << "m1_function1() = " << m1_function1() << endl;
    cout << "m1_function2() = " << m1_function2() << endl;

    cout << "m2_function1() = " << m2_function1() << endl;
    cout << "m2_function2() = " << m2_function2() << endl;
    
    cout << "Hello world!" << endl;
    return 0;
}